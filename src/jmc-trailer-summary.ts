import { LitElement, html, css, TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';

/**
 * Summarizes the details of a trailer
 */
@customElement('jmc-trailer-summary')
export class JmcTrailerSummary extends LitElement {
  static styles = css`
    :host {
      display: block;
    }

    p {
      margin: 0;
    }
  `;

  /**
   * JSON representation of a single trailer
   */
  @property({type: Object})
  trailer = function() { return {}; };

  render(): TemplateResult {
    return html`
      <div>
        <p><b>Trailer Number:</b> ${this.trailer.trailerNumber}</p>
        <p><b>Driver Call:</b> ${this.trailer.driverCall ? `Yes` : `No`}</p>
        <p><b>Driver Waiting:</b> ${this.trailer.driverWaiting ? `Yes` : `No`}</p>
        ${ this.trailer.driverCall || this.trailer.driverWaiting ? html`<p><b>Truck Number:</b> ${this.trailer.truckNumber}</p>` : html``}
        <p><b>Shoveling Tech:</b> ${this.trailer.shovelingTech ? html`${this.trailer.shovelingTech.firstName} ${this.trailer.shovelingTech.lastName}` : html``}</p>
        <!-- p><b>Shoveling Duration:</b> </p -->
        <p>&nbsp;</p>
        <p><b>Snow Conditions:</b> ${this.trailer.snowConditions ? html`${this.trailer.snowConditions.label}` : html``}</p>
        <p><b>Ice Conditions:</b> ${this.trailer.iceConditions ? html`${this.trailer.iceConditions.label}` : html``}</p>
        <p><b>Notes:</b> ${this.trailer.notes}</p>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'jmc-trailer-summary': JmcTrailerSummary;
  }
}


