import { LitElement, html, css, TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';

/**
 * Summarizes the details of a stop.
 *
 */
@customElement('jmc-stop-summary')
export class JmcStopSummary extends LitElement {
  static styles = css`
    :host {
      display: block;
    }
  `;

  /**
   * JSON representation of a single stop
   */
  @property({type: Object})
  stop = function() { return {}; };

  render(): TemplateResult {
    return html`
      ${this.stop.name}
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'jmc-stop-summary': JmcStopSummary;
  }
}


