import { JmcTrailerSummary } from '../jmc-trailer-summary';
import { fixture, html } from '@open-wc/testing';

const assert = chai.assert;

suite('jmc-trailer-summary', () => {
  test('is defined', () => {
    const el = document.createElement('jmc-trailer-summary');
    assert.instanceOf(el, JmcTrailerSummary);
  });

  test('renders with default values', async () => {
    const el = await fixture(html`<jmc-trailer-summary></jmc-trailer-summary>`);
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, World!</h1>
      <button part="button">Click Count: 0</button>
      <slot></slot>
    `
    );
  });

  test('renders with a set name', async () => {
    const el = await fixture(html`<jmc-trailer-summary name="Test"></jmc-trailer-summary>`);
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, Test!</h1>
      <button part="button">Click Count: 0</button>
      <slot></slot>
    `
    );
  });

  test('handles a click', async () => {
    const el = (await fixture(html`<jmc-trailer-summary></jmc-trailer-summary>`)) as JmcTrailerSummary;
    const button = el.shadowRoot!.querySelector('button')!;
    button.click();
    await el.updateComplete;
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, World!</h1>
      <button part="button">Click Count: 1</button>
      <slot></slot>
    `
    );
  });
});



