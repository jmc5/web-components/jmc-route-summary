import { JmcStopSummary } from '../jmc-stop-summary';
import { fixture, html } from '@open-wc/testing';

const assert = chai.assert;

suite('jmc-stop-summary', () => {
  test('is defined', () => {
    const el = document.createElement('jmc-stop-summary');
    assert.instanceOf(el, JmcStopSummary);
  });

  test('renders with default values', async () => {
    const el = await fixture(html`<jmc-stop-summary></jmc-stop-summary>`);
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, World!</h1>
      <button part="button">Click Count: 0</button>
      <slot></slot>
    `
    );
  });

  test('renders with a set name', async () => {
    const el = await fixture(html`<jmc-stop-summary name="Test"></jmc-stop-summary>`);
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, Test!</h1>
      <button part="button">Click Count: 0</button>
      <slot></slot>
    `
    );
  });

  test('handles a click', async () => {
    const el = (await fixture(html`<jmc-stop-summary></jmc-stop-summary>`)) as JmcStopSummary;
    const button = el.shadowRoot!.querySelector('button')!;
    button.click();
    await el.updateComplete;
    assert.shadowDom.equal(
      el,
      `
      <h1>Hello, World!</h1>
      <button part="button">Click Count: 1</button>
      <slot></slot>
    `
    );
  });
});



