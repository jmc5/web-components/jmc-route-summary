---
layout: example.11ty.cjs
title: <jmc-stop-summary> ⌲ Examples ⌲ Basic
tags: example
name: Basic
description: A basic example
---

<style>
  jmc-stop-summary p {
    border: solid 1px blue;
    padding: 8px;
  }
</style>
<jmc-stop-summary>
  <p>This is child content</p>
</jmc-stop-summary>

<h3>CSS</h3>

```css
  jmc-stop-summary p {
    border: solid 1px blue;
    padding: 8px;
  }
```

<h3>HTML</h3>

```html
<jmc-stop-summary>
  <p>This is child content</p>
</jmc-stop-summary>
```
