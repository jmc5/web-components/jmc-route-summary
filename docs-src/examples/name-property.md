---
layout: example.11ty.cjs
title: <jmc-stop-summary> ⌲ Examples ⌲ Name Property
tags: example
name: Name Property
description: Setting the name property
---

<jmc-stop-summary name="Earth"></jmc-stop-summary>

<h3>HTML</h3>

```html
<jmc-stop-summary name="Earth"></jmc-stop-summary>
```
