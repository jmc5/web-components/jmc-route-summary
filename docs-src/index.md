---
layout: page.11ty.cjs
title: <jmc-route-summary> ⌲ Home
---

# &lt;jmc-route-summary>

`<jmc-route-summary>` is a suite of webcomponents for Speechpad apps and pages. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec vitae fermentum lacus, sit amet feugiat enim. Quisque eu dapibus justo. Donec risus purus, malesuada sollicitudin massa ac,
vulputate ultrices libero. 

## As easy as HTML

<section class="columns">
  <div>

`<jmc-stop-summary>` is just an HTML element. You can it anywhere you can use HTML!

```html
<jmc-stop-summary></jmc-stop-summary>
```

  </div>
  <div>

<jmc-stop-summary></jmc-stop-summary>

  </div>
</section>

## Configure with attributes

<section class="columns">
  <div>

`<jmc-stop-summary>` can be configured with attributed in plain HTML.

```html
<jmc-stop-summary name="HTML"></jmc-stop-summary>
```

  </div>
  <div>

<jmc-stop-summary name="HTML"></jmc-stop-summary>

  </div>
</section>

## Declarative rendering

<section class="columns">
  <div>

`<jmc-stop-summary>` can be used with declarative rendering libraries like Angular, React, Vue, and lit-html

```js
import {html, render} from 'lit';

const name="lit";

render(html`
  <h2>This is a &lt;jmc-stop-summary&gt;</h2>
  <jmc-stop-summary .name=${name}></jmc-stop-summary>
`, document.body);
```

  </div>
  <div></div>
</section>
